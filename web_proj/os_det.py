import platform
import subprocess
import os
import sys

def run_command(command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    stdout, stderr = process.communicate()
    if process.returncode != 0:
        print(f"Error executing command: {command}")
        print(stderr.decode())
        sys.exit(1)
    return stdout.decode()

def check_openssl():
    try:
        run_command("openssl version")
    except:
        if platform.system() == "Linux":
            print("OpenSSL not found. Installing...")
            run_command("sudo apt-get update && sudo apt-get install -y openssl")
        elif platform.system() == "Windows":
            print("OpenSSL not found. Installing with Chocolatey...")
            run_command("choco install openssl")
        else:
            print("Unsupported operating system. Please install OpenSSL manually.")
            sys.exit(1)

def generate_certs():
    certs_dir = os.path.join(os.getcwd(), "certs")
    root_dir = os.path.join(certs_dir, "root")
    pki_dir = os.path.join(root_dir, "pki")

    os.makedirs(root_dir, exist_ok=True)
    os.makedirs(pki_dir, exist_ok=True)

    # Generate Root CA
    run_command(f"openssl genrsa -out {root_dir}/rootCA.key 4096")
    run_command(f"openssl req -new -x509 -days 365 -key {root_dir}/rootCA.key -out {root_dir}/rootCA.crt "
                f"-subj \"/C=RU/ST=Moscow/L=Moscow/O=Example Root CA/CN=Example Root CA\"")

    # Generate Intermediate CA
    run_command(f"openssl genrsa -out {root_dir}/intermediate.key 4096")
    run_command(f"openssl req -new -key {root_dir}/intermediate.key -out {root_dir}/intermediate.csr "
                f"-subj \"/C=RU/ST=Moscow/L=Moscow/O=Example Intermediate CA/CN=Example Intermediate CA\"")
    run_command(f"openssl x509 -req -days 365 -in {root_dir}/intermediate.csr -CA {root_dir}/rootCA.crt "
                f"-CAkey {root_dir}/rootCA.key -CAcreateserial -out {root_dir}/intermediate.crt")

    # Generate Web2 certificate
    run_command(f"openssl genrsa -out {pki_dir}/web2.key 2048")
    run_command(f"openssl req -new -key {pki_dir}/web2.key -out {pki_dir}/web2.csr "
                f"-subj \"/C=RU/ST=Moscow/L=Moscow/O=Example Web2/CN=web2.example.com\" -extensions req_ext")
    run_command(f"openssl x509 -req -days 365 -in {pki_dir}/web2.csr -CA {root_dir}/intermediate.crt "
                f"-CAkey {root_dir}/intermediate.key -CAcreateserial -out {pki_dir}/web2.crt")

    # Generate Web3 certificate
    run_command(f"openssl genrsa -out {pki_dir}/web3.key 2048")
    run_command(f"openssl req -new -key {pki_dir}/web3.key -out {pki_dir}/web3.csr "
                f"-subj \"/C=RU/ST=Moscow/L=Moscow/O=Example Web3/CN=web3.example.com\"")
    run_command(f"openssl x509 -req -days 365 -in {pki_dir}/web3.csr -CA {root_dir}/intermediate.crt "
                f"-CAkey {root_dir}/intermediate.key -CAcreateserial -out {pki_dir}/web3.crt")

    # Generate Client certificate
    run_command(f"openssl genrsa -out {pki_dir}/client.key 2048")
    run_command(f"openssl req -new -key {pki_dir}/client.key -out {pki_dir}/client.csr "
                f"-subj \"/C=RU/ST=Moscow/L=Moscow/O=Example Client/CN=Example Client\"")
    run_command(f"openssl x509 -req -days 365 -in {pki_dir}/client.csr -CA {root_dir}/intermediate.crt "
                f"-CAkey {root_dir}/intermediate.key -CAcreateserial -out {pki_dir}/client.crt")

    print("Certificates generated successfully.") 
    
if __name__ == "__main__":
    check_openssl()
    generate_certs()