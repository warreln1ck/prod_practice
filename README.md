# prod_practice

1. Запуск проекта:
   - Открыть терминал в корневой директории проекта.
   - Выполнить команду:
     ```
     docker-compose up --build
     ```
   - Эта команда соберет образы для веб-сервисов и запустит все контейнеры.

2. Проверка работоспособности:
   - Добавить следующие строки в файл hosts (что на Linux, что на Windows он присутствует):
     ```
     127.0.0.1 web1.example.com web2.example.com web3.example.com
     ```
   - Протестировать каждый сервис:
     - HTTP (web1): `curl http://web1.example.com`
     - HTTPS (web2): `curl https://web2.example.com --cacert certs/root/rootCA.crt`
     - HTTPS с mTLS (web3): `curl https://web3.example.com --cacert certs/root/rootCA.crt --cert certs/root/pki/client.crt --key certs/root/pki/client.key`

3. Остановка проекта:
   - Когда заканчиваете тестирование, восстановить контейнеры можно командой:
     ```
     docker-compose down
     ```
openssl req -new -key pki/web2.key -out pki/web2.csr -subj "/C=RU/ST=Moscow/L=Moscow/O=Example Web2/CN=web2.example.com" 
openssl x509 -req -days 365 -in pki/web2.csr -CA ./intermediate.crt -CAkey ./intermediate.key -CAcreateserial -out pki/web22.crt -extensions req_ext2 -extfile /etc/ssl/openssl.cnf